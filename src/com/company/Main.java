package com.company;

import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("введите первое число");
        int num1 = getInt();
        System.out.println("введите второе число");
        int num2 = getInt();
        int result = calculateSumAccordingToSpecificRules(num1, num2);
        System.out.println("Результат операции: " + result);
    }

    private static int calculateSumAccordingToSpecificRules(int num1, int num2) {
        int result;
        if (num1 != num2) {
            result = num1 + num2;
        } else {
            result = (num1 + num2) * 2;
        }
        return result;
    }

    public static int getInt() {
        return scanner.nextInt();
    }


}
